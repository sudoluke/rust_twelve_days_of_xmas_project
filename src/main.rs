fn main() {
    // Define days, gifts, and count variables
    let days = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "nineth", "tenth", "eleventh", "twelfth"];
    let gifts = ["A partridge in a pear tree", "Two turtle doves, and", "Three french hens", "Four calling birds", "Five golden rings", "Six geese a-laying", "Seven swans a -swimming", "Eight maids a-milking", "Nine ladies dancing", "Ten lords a-leaping", "Eleven pipers piping", "Twelve drummers drumming"];
    let mut count = 0;
    //iterate over count from 0 to 12
    while count < 12 {
        //print the day and gift in the index by count
        println!("On the {} day of Christmas, my true love gave to me", days[count]);
        println!("{}", gifts[count]);

        // print the gifts from previous days in reverse order
        // Add the into_iter() method to make the gifts iterable
        // Add .rev() to list in revers order
        for gift in gifts[0..count].into_iter().rev() {
            println!("{}", gift);
        }
    // increment the count and separate days with a new line
    count += 1;
    println!(" ");
       
    }
}
 
